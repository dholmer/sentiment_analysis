import pandas as pd
import pickle
import numpy as np
from collections import Counter

import matplotlib.pyplot as plt

from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import classification_report

from sklearn.feature_extraction.text import TfidfVectorizer

from nltk.collocations import *
import collections
import nltk

from nltk.tokenize import word_tokenize

import seaborn as sns


class Sent():


    def __init__(self):
        self.training_data = self.load_training()
        self.test_data = self.load_test()
        self.classifications = []
        self.clf = None
        self.vectorizer = None
        self.pos_words = []
        self.neg_words = []
    def load_training(self):

        training_data = pd.read_pickle("training_df")
        return training_data

    def load_test(self):

        test_data = pd.read_pickle("test_df")
        return test_data

    def show_training_data(self):
        for index, review in enumerate(self.training_data["text"][:10]):
            review = review.replace("<br /><br />","")
            print(review)
            print(f"REVIEW TAGGED AS {self.training_data['Category'][index].upper()}")
            print("------------------")

    def most_common_words(self):
        word_counts = Counter(word.lower() for review in self.training_data["tokenized"] for word in review)
        plt.figure(figsize=(20,10))
        plt.bar(*zip(*word_counts.most_common(20)))

    def most_common_words_v2(self):
        #word_counts = Counter(word.lower() for review in self.training_data["cleaned"] for word in review)
        corpus = []

        word_counts = Counter(word.lower() for review in self.training_data["cleaned"] for word in review)

        plt.figure(figsize=(20,10))
        plt.bar(*zip(*word_counts.most_common(20)))

    def count_word(self):
        word=input()
        word_counts = Counter(word.lower() for review in self.training_data["cleaned"] for word in review)
        if word.lower() in word_counts:
            return f"{word.lower()} was found: {word_counts[word.lower()]} times in the dataset"
        else:
            return "word not found"

    def word_distribution(self):
        #training_data=load_training()
        word = input()
        pos_counts = Counter(word.lower() for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"]) for word in review if cat == "pos")
        neg_counts = Counter(word.lower() for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"]) for word in review if cat == "neg")
        self.pos_words = [word[0] for word in pos_counts.most_common(250)]
        self.neg_words = [word[0] for word in neg_counts.most_common(250)]
        if word.lower() in pos_counts or word.lower() in neg_counts:
            if pos_counts[word.lower()] > neg_counts[word.lower()]:
                percent_string = f"The word is found {round((pos_counts[word.lower()] / neg_counts[word.lower()])-1,2)} times more often in the positive reviews than in the negative reviews"
            else:
                percent_string = f"The word is found {round((neg_counts[word.lower()] / pos_counts[word.lower()])-1,2)} times more often in the negative reviews than in the positive reviews"
            return f"Times found in positive reviews: {pos_counts[word.lower()]}. Times found in negative reviews: {neg_counts[word.lower()]}. {percent_string}"
        else:
            return "word not found in the reviews"

    def most_common_pos(self):
        #training_data=load_training()
        #pos_counts = Counter(word.lower() for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"]) for word in review if cat == "pos")

        unique_pos = []
        if self.pos_words == []:
            print("Du måste köra föregående celler för att detta ska fungera!")
        else:

            for word in self.pos_words:
                if word not in self.neg_words:
                    unique_pos.append(word)
        print(", ".join(unique_pos))

    def most_common_neg(self):
        #training_data=load_training()
        #neg_counts = Counter(word.lower() for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"]) for word in review if cat == "neg")

        unique_neg = []
        if self.neg_words == []:
            print("Du måste köra föregående celler för att detta ska fungera!")
        else:

            for word in self.neg_words:
                if word not in self.pos_words:
                    unique_neg.append(word)
        print(", ".join(unique_neg))

    def most_freq(self):
        neg = [word[0] for word in self.most_common_neg()]
        pos = [word[0] for word in self.most_common_pos()]

        unique_neg = []
        unique_pos = []
        for p, n in zip(pos,neg):
            if n not in pos:
                unique_neg.append(n)
            if p not in neg:
                unique_pos.append(p)
        print(unique_neg)
        print(unique_pos)

    def most_freq_words_v2(self):

        vectorizer = TfidfVectorizer()
        X = vectorizer.fit_transform(corpus)
        print(vectorizer.get_feature_names())

        print(X.shape)

    def most_freq_words(self, category):
        vectorizer = TfidfVectorizer(max_df = 0.9, stop_words = "english")
        #new_list = [word for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"]) for word in review if cat == "neg"]
        vectors = vectorizer.fit_transform(
            [' '.join(word.lower() for word in review if cat == "neg") for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"])] ,
            [' '.join(word.lower() for word in review if cat == "pos") for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"])])


        pos = [' '.join(word.lower() for word in review if cat == category) for review, cat in zip(self.training_data["cleaned"], self.training_data["Category"])]
    #    print(pos)
        feature_array = np.array(vectorizer.get_feature_names())

        doc = vectorizer.transform(pos)
        #print(pos)
        top_n = 15

    #    feature_array = np.array(vectorizer.get_feature_names())


        #tfidf_sorting = np.argsort(doc.toarray()).flatten()[::-1]


        #n = 50
        #top_n = feature_array[tfidf_sorting][:n]
        #print(top_n)
        #print(tfidf_sorting)

        """
        feature_names = vectorizer.get_feature_names()
        dense = vectors.todense()
        denselist = dense.tolist()
        df = pd.DataFrame(denselist, columns=feature_names)
        df['max_value'] = df.max(axis=1)
        print(df['awful'])"""
        #print(denselist)
        #tfidf_sorting = np.argsort(doc).flatten()[::-1]
        #tfidf_sorting = np.argsort(doc[::-1])
        #tfidf_sorting = np.argsort(vectors)[:-(top_n+1):-1]
    #    print(feature_array[tfidf_sorting][:n])
    #    top_n = 100
    ##    def get_top_tf_idf_words(response, top_n=5):
        #
        sorted_nzs = np.argsort(doc.data)[:-(top_n+1):-1]
        print( feature_array[doc.indices[sorted_nzs]])
    #    print([get_top_tf_idf_words(response,5) for response in doc])
        #return vectors.shape[0] * vectors.shape[1] * vectors.dtype.itemsize / 1e6
        #feature_names = vectorizer.get_feature_names()
        #dense = vectors.todense()
        #denselist = dense.tolist()
        #return pd.DataFrame(denselist, columns=feature_names)

    def most_freq_words_v3(self):
        pass

    def find_collocations(self, word, number_to_show = 10):

        finder = TrigramCollocationFinder.from_words(
        [word for review in self.training_data["cleaned"] for word in review])

        trigram_measures = nltk.collocations.TrigramAssocMeasures()
        filter = lambda *w: word.lower() not in w

        # only trigrams that appear 3+ times
        #finder.apply_freq_filter(3)

        # only trigrams that contain 'creature'
        finder.apply_ngram_filter(filter)
        # return the 10 n-grams with the highest PMI
        print (finder.nbest(trigram_measures.pmi, number_to_show))

    def naive_bayes_classifier_test(self):
        x_test = self.test_data["cleaned"]
        y_test = self.test_data["Category"]

        test_x =self.vectorizer.transform(x_test)
        y_pred_classes = self.clf.predict(test_x)


        #print(classification_report(y_test, y_pred_classes))
        self.classifications = y_pred_classes
    #    self.prediction_scores()

    def naive_bayes_classifier_train(self):

        x_training = self.training_data["cleaned"]
        y_training = self.training_data["Category"]
        x_test = self.test_data["cleaned"]
        y_test = self.test_data["Category"]

        self.vectorizer = CountVectorizer(tokenizer=lambda doc: doc, lowercase=False)
        X = self.vectorizer.fit_transform(x_training)

        self.clf = MultinomialNB()
        self.clf.fit(X, y_training)
        print("Successfully trained the classifier!")
        self.naive_bayes_classifier_test()




    def prediction_scores(self):
    #    print(self.classifications)
        y_test = self.test_data["Category"]
        TP = 0
        TN = 0
        FP = 0
        FN = 0
        for i in range(len(self.classifications)):
            if self.classifications[i] == "pos" and y_test[i] == "pos":
                TP +=1

            elif self.classifications[i] == "neg" and y_test[i] == "neg":
                TN +=1

            elif self.classifications[i] == "pos" and y_test[i] == "neg":
                FP +=1

            elif self.classifications[i] == "neg" and y_test[i] == "pos":
                FN +=1

        pos_precision = TP / (TP + FP)
        pos_recall = TP / (TP + FN)
        neg_precision = TN / (TN + FN)
        neg_recall = TN / (TN + FP)
        print(f"TP: {TP}, TN: {TN}, FP: {FP}, FN: {FN} tot: {len(self.classifications)} ")
        print(f"PP: {pos_precision}, PR: {pos_recall}, NP: {neg_precision}, NR: {neg_recall}")
        self.confusion_matrix()

    def confusion_matrix(self):
        from sklearn.metrics import confusion_matrix

        plt.figure(figsize=(4,4))


        labels = list(self.test_data["Category"].unique())
        labels.sort()
        sns.heatmap(confusion_matrix(self.test_data["Category"], self.classifications),cbar=False,annot=True,square=True,xticklabels=labels,yticklabels=labels,fmt='d', cmap="Greens")

        plt.xlabel('Predicted')
        plt.ylabel('Actual')
        plt.yticks(rotation=0)
        plt.show()

    def show_classifications(self):

        ## change to pick the span of reviews to show
        start, end = 110, 120

        for index, review in enumerate(self.test_data["text"][start:end]):

            print(f"{index + 1} - Manually tagged as: {self.test_data['Category'][start+index].upper()} | Predicted by classifier as: {self.classifications[start+index].upper()} |")
            review = review.replace("<br /><br />","")
            print(review)
            print("---------------------------------")

    def show_errors(self):

        num_of_errors = 0

        for index, review in enumerate(self.test_data["text"]):
            if self.test_data['Category'][index] != self.classifications[index]:

                print(f"Manually tagged as: {self.test_data['Category'][index].upper()} | Predicted by classifier as: {self.classifications[index].upper()} |")
                review = review.replace("<br /><br />","")
                print(review)
                print("---------------------------------")

                num_of_errors +=1

                if num_of_errors == 10:
                    break
if __name__ == "__main__":
    s = Sent()
    s.naive_bayes_classifier_train()
    s.naive_bayes_classifier_test()
    s.show_classifications()
